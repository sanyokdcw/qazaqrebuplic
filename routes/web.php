<?php

use Illuminate\Support\Facades\Route;
use App\Models\{
    Banner,
    Product,
    Location,
    Phone
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $banner = Banner::first();
    $products = Product::all();
    $locations = Location::all();
    $phones = Phone::all();
    return view('index', compact('banner', 'products', 'locations', 'phones'));
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
